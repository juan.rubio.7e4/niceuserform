package cat.itb.dam.m07.niceuserform.model;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserViewModel extends ViewModel {

    List<User> users;

    public UserViewModel(){
        users = new ArrayList<>();

        users.add(new User("Victor92", "12345678", "victor@gmail.com", "Victor", "Gomez Perez", new Date(), "She", true));
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void addUser(User user){
        users.add(user);
    }
}
