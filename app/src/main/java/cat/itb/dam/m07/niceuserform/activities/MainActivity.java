package cat.itb.dam.m07.niceuserform.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import cat.itb.dam.m07.niceuserform.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}