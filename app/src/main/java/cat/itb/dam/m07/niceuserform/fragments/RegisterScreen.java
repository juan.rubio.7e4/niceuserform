package cat.itb.dam.m07.niceuserform.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cat.itb.dam.m07.niceuserform.R;
import cat.itb.dam.m07.niceuserform.model.User;
import cat.itb.dam.m07.niceuserform.model.UserViewModel;

public class RegisterScreen extends Fragment {

    TextInputLayout usernameInputLayout;
    TextInputEditText usernameEditText;

    TextInputLayout passwordInputLayout;
    TextInputEditText passwordEditText;

    TextInputLayout repeatPasswordInputLayout;
    TextInputEditText repeatPasswordEditText;

    TextInputLayout emailInputLayout;
    TextInputEditText emailEditText;

    TextInputLayout nameInputLayout;
    TextInputEditText nameEditText;

    TextInputLayout surnameInputLayout;
    TextInputEditText surnameEditText;

    TextInputLayout birthInputLayout;
    TextInputEditText birthEditText;

    TextInputLayout genderInputLayout;
    AutoCompleteTextView genderAutoComplete;

    CheckBox terms;

    Button register;
    Button login;

    List<String> listGenders;
    ArrayAdapter<String> arrayAdapterGenders;

    UserViewModel usersViewModel;
    User user;

    NavController navController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_screen, container, false);

        usernameInputLayout = view.findViewById(R.id.usernameInputLayoutRegister);
        usernameEditText = view.findViewById(R.id.usenameEditTextRegister);

        passwordInputLayout = view.findViewById(R.id.passwordInputLayoutRegister);
        passwordEditText = view.findViewById(R.id.passwordEditTextRegister);

        repeatPasswordInputLayout = view.findViewById(R.id.repeatPasswordInputLayoutRegister);
        repeatPasswordEditText = view.findViewById(R.id.repeatPasswordEditTextRegister);

        emailInputLayout = view.findViewById(R.id.emailInputLayoutRegister);
        emailEditText = view.findViewById(R.id.emailEditTextRegister);

        nameInputLayout = view.findViewById(R.id.nameInputLayoutRegister);
        nameEditText = view.findViewById(R.id.nameEditTextRegister);

        surnameInputLayout = view.findViewById(R.id.surnameInputLayoutRegister);
        surnameEditText = view.findViewById(R.id.surnameEditTextRegister);

        birthInputLayout = view.findViewById(R.id.birthInputLayoutRegister);
        birthEditText = view.findViewById(R.id.birthEditTextRegister);

        terms = view.findViewById(R.id.termsCheckBox);

        register = view.findViewById(R.id.registerButtonRegister);
        login = view.findViewById(R.id.loginButtonRegister);

        genderInputLayout = view.findViewById(R.id.genderInputLayoutRegister);
        genderAutoComplete = view.findViewById(R.id.genderAutoCompleteTextView);

        listGenders = new ArrayList<>();
        listGenders.add("She");
        listGenders.add("He");
        listGenders.add("They");
        listGenders.add("Prefer not to say");

        arrayAdapterGenders = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, listGenders);
        genderAutoComplete.setAdapter(arrayAdapterGenders);

        usersViewModel = new ViewModelProvider(getActivity()).get(UserViewModel.class);
        user = new User();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        passwordEditText.setOnKeyListener(new EditText.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN){
                    if (passwordEditText.getText().length() < 7){
                        passwordInputLayout.setHelperText("The password must have a minimum of 8 characters");
                    }else {
                        passwordInputLayout.setHelperText(null);
                    }
                }
                return false;
            }
        });

        repeatPasswordEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (!passwordEditText.getText().toString().equals(repeatPasswordEditText.getText().toString())){
                    repeatPasswordInputLayout.setHelperText("Passwords are not the same");
                }else {
                    repeatPasswordInputLayout.setHelperText(null);
                }
                return false;
            }
        });

        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText("Select your Birth Date");
        final MaterialDatePicker<Long> materialDatePicker = builder.build();

        birthEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialDatePicker.show(getParentFragmentManager(), "MATERIAL_DATE_PICKER");
                materialDatePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
                    @Override
                    public void onPositiveButtonClick(Long selection) {
                        Date date = new Date(selection);
                        user.setBirthDate(date);

                        DateFormat format =new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                        String localDate=format.format(date);
                        birthEditText.setText(localDate);
                    }
                });
            }
        });

        navController = Navigation.findNavController(view);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRegister();

            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(RegisterScreenDirections.actionRegisterScreenToLoginScreen());
            }
        });
    }

    public void setRegister(){
        if (usernameEditText.getText().toString().isEmpty() ||
                passwordEditText.getText().length() < 8 ||
                !repeatPasswordEditText.getText().toString().equals(passwordEditText.getText().toString()) ||
                emailEditText.getText().toString().isEmpty() ||
                nameEditText.getText().toString().isEmpty() ||
                surnameEditText.getText().toString().isEmpty() ||
                birthEditText.getText().toString().isEmpty() ||
                genderAutoComplete.getText().toString().isEmpty() ||
                !terms.isChecked()){

            if (usernameEditText.getText().toString().isEmpty()){
                usernameInputLayout.setError("Required field");
            }else {
                usernameInputLayout.setError(null);
            }
            if(passwordEditText.getText().length() < 8){
                passwordInputLayout.setError("The password must have a minimum of 8 characters");
            }else {
                passwordInputLayout.setError(null);
            }
            if (!repeatPasswordEditText.getText().toString().equals(passwordEditText.getText().toString())){
                repeatPasswordInputLayout.setError("Passwords are not the same ");
            }else {
                repeatPasswordInputLayout.setError(null);
            }
            if (emailEditText.getText().toString().isEmpty()){
                emailInputLayout.setError("Required field");
            }else {
                emailInputLayout.setError(null);
            }
            if (nameEditText.getText().toString().isEmpty()){
                nameInputLayout.setError("Required field");
            }else {
                nameInputLayout.setError(null);
            }
            if (surnameEditText.getText().toString().isEmpty()){
                surnameInputLayout.setError("Required field");
            }else {
                surnameInputLayout.setError(null);
            }
            if (birthEditText.getText().toString().isEmpty()){
                birthInputLayout.setError("Required field");
            }else {
                birthInputLayout.setError(null);
            }
            if (genderAutoComplete.getText().toString().isEmpty()){
                genderInputLayout.setError("Required field");
            }else {
                genderInputLayout.setError(null);
            }
            if (!terms.isChecked()){ ;
                Toast.makeText(getContext(), "If you want to register, you have to accept the terms and conditions ", Toast.LENGTH_SHORT).show();
            }

        }else {
            user.setUsername(usernameEditText.getText().toString());
            user.setPassword(passwordEditText.getText().toString());
            user.setEmail(emailEditText.getText().toString());
            user.setName(nameEditText.getText().toString());
            user.setSurnames(surnameEditText.getText().toString());
            user.setTerms(terms.isChecked());
            usersViewModel.addUser(user);
            navController.navigate(RegisterScreenDirections.actionRegisterScreenToWelcomeScreen());
        }
    }

}