package cat.itb.dam.m07.niceuserform.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import cat.itb.dam.m07.niceuserform.R;
import cat.itb.dam.m07.niceuserform.model.User;
import cat.itb.dam.m07.niceuserform.model.UserViewModel;

public class LoginScreen extends Fragment {

    TextInputLayout usernameInputLayout;
    TextInputEditText usernameEditText;
    TextInputLayout passwordInputLayout;
    TextInputEditText passwordEditText;
    Button loginButton;
    Button registerButton;
    Button forgotButton;
    UserViewModel users;

    NavController navController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_screen, container, false);

        users = new ViewModelProvider(getActivity()).get(UserViewModel.class);

        usernameInputLayout = view.findViewById(R.id.usernameInputLayout);
        usernameEditText = view.findViewById(R.id.usernameEditText);
        passwordInputLayout = view.findViewById(R.id.passwordInputLayout);
        passwordEditText = view.findViewById(R.id.passwordEditText);

        loginButton = view.findViewById(R.id.loginButton);
        registerButton = view.findViewById(R.id.registerButton);
        forgotButton = view.findViewById(R.id.forgotButton);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);

        usernameEditText.setOnKeyListener(new EditText.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                usernameInputLayout.setErrorEnabled(false);
                return false;
            }
        });

        passwordEditText.setOnKeyListener(new EditText.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                passwordInputLayout.setErrorEnabled(false);
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
    }

    public void login(){
        if (usernameEditText.getText().toString().isEmpty()){
            usernameInputLayout.setError("This field can't be empty");
        }else {
            if (passwordEditText.getText().toString().isEmpty()){
                passwordInputLayout.setError("This field can't be empty");
            }else {
                for (User u: users.getUsers()) {
                    if (u.getUsername().equals(usernameEditText.getText().toString()) &&
                            u.getPassword().equals(passwordEditText.getText().toString())){

                        navController.navigate(LoginScreenDirections.actionLoginScreenToWelcomeScreen());
                    }else {
                        usernameInputLayout.setError("Username does not exist or the password is wrong");
                        passwordInputLayout.setError("The password is wrong");
                    }
                }
            }
        }
    }

    public void register(){
        navController.navigate(LoginScreenDirections.actionLoginScreenToRegisterScreen());
    }
}